import 'package:dartz/dartz.dart';
import 'package:hobbit_tracker/core/error/failure.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/moor_database.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';

abstract class HobbitTrackerRepository {
  Stream<List<HobbitEntity>> getListOfHobbitTracker();
  Future<Either<Failures, bool>> setDoneHobbitTracker(HobbitEntity hobbit);
  Future<Either<Failures, bool>> addNewTracker(Hobbit hobbit);
  Future<Either<Failures, bool>> deleteHobbitTracker(HobbitEntity hobbit);
}
