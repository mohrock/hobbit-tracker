import 'package:dartz/dartz.dart';
import 'package:hobbit_tracker/core/error/failure.dart';
import 'package:hobbit_tracker/core/usecases/usecase.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/moor_database.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/domain/repositories/hobbit_tracker_repository.dart';

class SetDoneHobbitTracker {
  final HobbitTrackerRepository repository;

  SetDoneHobbitTracker(this.repository);

  @override
  Future<Either<Failures, bool>> call(HobbitEntity hobbit) async {
    return await repository.setDoneHobbitTracker(hobbit);
  }
}