import 'package:dartz/dartz.dart';
import 'package:hobbit_tracker/core/error/failure.dart';
import 'package:hobbit_tracker/core/usecases/usecase.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/domain/repositories/hobbit_tracker_repository.dart';

class AddNewTracker implements UseCase<bool, Hobbit> {
  final HobbitTrackerRepository repository;

  AddNewTracker(this.repository);

  @override
  Future<Either<Failures, bool>> call(Hobbit hobbit) async {
    return await repository.addNewTracker(hobbit);
  }
}