import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Hobbit extends Equatable {
  final bool isGood;
  final String title;
  final int period;
  final int hourIndex;

  Hobbit({
    @required this.isGood,
    @required this.title,
    @required this.hourIndex,
    @required this.period,
  });

  @override
  List<Object> get props => [isGood, title, period];
}
