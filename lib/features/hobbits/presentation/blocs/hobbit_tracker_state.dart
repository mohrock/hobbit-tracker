part of 'hobbit_tracker_cubit.dart';

@immutable
abstract class HobbitTrackerState {}

class Empty extends HobbitTrackerState {}

class Success extends HobbitTrackerState {
  final List<HobbitDTO> hobbits;

  Success({@required this.hobbits});

  @override
  List<Object> get props => [hobbits];
}

