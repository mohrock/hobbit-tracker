import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:hobbit_tracker/core/usecases/usecase.dart';
import 'package:hobbit_tracker/core/utils/time_controller.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/add_new_tracker.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/deleteHobbitTracker.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/get_list_of_hobbit_tracker.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/set_done_hobbit_tracker.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/DTOs/hobbit_dto.dart';
import 'package:meta/meta.dart';

part 'hobbit_tracker_state.dart';

class HobbitTrackerCubit extends Cubit<HobbitTrackerState> {
  final GetListOfHobbitTracker getListOfHobbitTracker;
  final AddNewTracker addNewTracker;
  final DeleteHobbitTracker deleteHobbitTracker;
  final SetDoneHobbitTracker setDoneHobbitTracker;
  final TimeController timeController;

  HobbitTrackerCubit({
    @required GetListOfHobbitTracker getListOfHobbitTracker,
    @required AddNewTracker addNewTracker,
    @required DeleteHobbitTracker deleteHobbitTracker,
    @required SetDoneHobbitTracker setDoneHobbitTracker,
    @required TimeController timeController,
  })  : this.getListOfHobbitTracker = getListOfHobbitTracker,
        this.deleteHobbitTracker = deleteHobbitTracker,
        this.setDoneHobbitTracker = setDoneHobbitTracker,
        this.timeController = timeController,
        this.addNewTracker = addNewTracker,
        super(Empty());

  Future<void> getHobbits() async {
    emit(Empty());
    List<HobbitDTO> output = [];
    getListOfHobbitTracker(NoParams()).forEach((element) {
      for (int i = 0; i < element.length; i++) {
        final nowHour =
            timeController.getHourSinceEpochOfDateTime(DateTime.now());
        final hourAgo = nowHour - element[i].hourIndex;
        final period = 1 - (hourAgo / element[i].period);
        var colorState = Colors.green;
        if (period < 0.20)
          colorState = Colors.red;
        else if (period < 0.40)
          colorState = Colors.orange;
        else if (period < 0.60)
          colorState = Colors.yellow;
        else if (period < 0.80)
          colorState = Colors.lightGreen;
        else
          colorState = Colors.green;
        output.add(HobbitDTO(
            hobbit: element[i],
            hourAgo: hourAgo,
            period: element[i].period,
            statePercent: period,
            stateColor: colorState,
            title: element[i].name));
      }
      emit(Success(hobbits: output));
    });
  }

  Future<void> addNewHobbit(String title, int period) async {
    Hobbit hobbit = Hobbit(
        isGood: true,
        title: title,
        hourIndex: timeController.getHourSinceEpochOfDateTime(DateTime.now()),
        period: period);

    await addNewTracker(hobbit);
    getHobbits();
  }

  Future<void> setDoneOneHobbit(HobbitDTO hobbit) async {
    await setDoneHobbitTracker(hobbit.hobbit);
    getHobbits();
  }

  Future<void> removeOneHobbit(HobbitDTO hobbit) async {
    await deleteHobbitTracker(hobbit.hobbit);
    getHobbits();
  }
}
