import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/moor_database.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:meta/meta.dart';

class HobbitDTO extends Equatable {
  final String title;
  final int hourAgo;
  final int period;
  final double statePercent;
  final Color stateColor;
  final HobbitEntity hobbit;

  HobbitDTO({
    @required this.hobbit,
    @required this.title,
    @required this.hourAgo,
    @required this.period,
    @required this.statePercent,
    @required this.stateColor,
  });

  @override
  List<Object> get props => [title, hourAgo, statePercent, stateColor];
}
