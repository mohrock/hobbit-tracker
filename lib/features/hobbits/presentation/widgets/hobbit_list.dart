import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/DTOs/hobbit_dto.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/widgets/hobbit_item.dart';

class HobbitList extends StatelessWidget {
  final List<HobbitDTO> hobbits;

  const HobbitList({
    Key key,
    @required this.hobbits,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        reverse: false,
        scrollDirection: Axis.vertical,
        itemCount: hobbits.length,
        itemBuilder: (BuildContext context, int index) {
          return HobbitItem(hobbit: hobbits[index]);
        }
    );
  }
}
