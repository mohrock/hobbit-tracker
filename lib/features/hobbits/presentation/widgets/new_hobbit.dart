import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/blocs/hobbit_tracker_cubit.dart';
import 'package:numberpicker/numberpicker.dart';

class NewTaskInput extends StatefulWidget {
  const NewTaskInput({
    Key key,
  }) : super(key: key);

  @override
  _NewTaskInputState createState() => _NewTaskInputState();
}

class _NewTaskInputState extends State<NewTaskInput> {
  DateTime newTaskDate;
  TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _buildTextField(context),
          _buildNumberPicker(context),
        ],
      ),
    );
  }

  Expanded _buildTextField(BuildContext context) {
    return Expanded(
      child: TextField(
        controller: controller,
        decoration: InputDecoration(hintText: 'enter new Hobbit'),
        onSubmitted: (inputName) {
          final hobbitCubit = BlocProvider.of<HobbitTrackerCubit>(context);
          hobbitCubit.addNewHobbit(inputName, _currentValue);
          resetValuesAfterSubmit();
        },
      ),
    );
  }

  int _currentValue = 24;

  NumberPicker _buildNumberPicker(BuildContext context) {
    return NumberPicker(
      value: _currentValue,
      minValue: 0,
      maxValue: 500,
      itemHeight: 30,
      itemWidth: 50,
      step: 8,
      axis: Axis.vertical,
      onChanged: (value) => setState(() => _currentValue = value),
    );
  }

  void resetValuesAfterSubmit() {
    setState(() {
      newTaskDate = null;
      controller.clear();
    });
  }
}
