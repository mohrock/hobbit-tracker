import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/DTOs/hobbit_dto.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/blocs/hobbit_tracker_cubit.dart';

class HobbitItem extends StatelessWidget {
  final HobbitDTO hobbit;

  const HobbitItem({
    Key key,
    @required this.hobbit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 60,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(3, 3),
          )
        ],
      ),
      child: Row(mainAxisSize: MainAxisSize.max, children: [
        Text(
          hobbit.title,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 15, color: Colors.black54),
        ),
        Text(
          "("+hobbit.hourAgo.toString()+"/"+hobbit.period.toString()+")",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.normal, fontSize: 13, color: Colors.black54),
        ),
        SizedBox(
          width: 15,
        ),
        Flexible(
          flex: 1,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: LinearProgressIndicator(
              minHeight: 10,
              value: hobbit.statePercent,
              valueColor: AlwaysStoppedAnimation<Color>(hobbit.stateColor),
              backgroundColor: Color(0xFFf2f2f2),
            ),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        IconButton(
          icon: Icon(
            Icons.check,
            color: Colors.green,
            size: 25.0,
          ),
          onPressed: () async {
            final hobbitCubit = BlocProvider.of<HobbitTrackerCubit>(context);
            hobbitCubit.setDoneOneHobbit(hobbit);
          },
        ),
        IconButton(
          icon: Icon(
            Icons.delete,
            color: Colors.black26,
            size: 25.0,
          ),
          onPressed: () async {
            final hobbitCubit = BlocProvider.of<HobbitTrackerCubit>(context);
            hobbitCubit.removeOneHobbit(hobbit);
          },
        ),
      ]),
    );
  }
}
