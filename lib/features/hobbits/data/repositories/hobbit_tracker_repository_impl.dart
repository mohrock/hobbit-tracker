import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:hobbit_tracker/core/error/failure.dart';
import 'package:hobbit_tracker/core/utils/time_controller.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/moor_database.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/storage_hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/data/models/hobbit_model.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/domain/repositories/hobbit_tracker_repository.dart';

class HobbitTrackerRepositoryImpl extends HobbitTrackerRepository {
  final AppDatabase appDatabase;
  final TimeController timeController;

  HobbitTrackerRepositoryImpl(this.appDatabase, this.timeController);

  @override
  Future<Either<Failures, bool>> addNewTracker(Hobbit hobbit) async {
    HobbitEntity hobbitEntity = HobbitEntity(
        name: hobbit.title,
        period: hobbit.period,
        hourIndex: hobbit.hourIndex,
        isGood: hobbit.isGood);
    await appDatabase.insertHobbit(hobbitEntity);
    return Right(true);
  }

  @override
  Future<Either<Failures, bool>> deleteHobbitTracker(HobbitEntity hobbit) async {
    await appDatabase.deleteHobbit(hobbit);
    return Future.value(Right(false));
  }

  @override
  Stream<List<HobbitEntity>> getListOfHobbitTracker() {
    return appDatabase.watchAllHobbits();
  }

  @override
  Future<Either<Failures, bool>> setDoneHobbitTracker(
      HobbitEntity hobbit) async {
    HobbitEntity newHobbitEntity = HobbitEntity(
        id: hobbit.id,
        name: hobbit.name,
        period: hobbit.period,
        hourIndex: timeController.getHourSinceEpochOfDateTime(DateTime.now()),
        isGood: hobbit.isGood);
    await appDatabase.updateHobbit(newHobbitEntity);
    return Future.value(Right(false));
  }
}
