import 'package:hobbit_tracker/features/hobbits/data/datasorces/moor_database.dart';
import 'package:meta/meta.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';

class HobbitModel extends Hobbit {
  HobbitModel({
    @required bool isGood,
    @required String title,
    @required int hourIndex,
    @required int period,
  }) : super(
            isGood: isGood, title: title, hourIndex: hourIndex, period: period);

  factory HobbitModel.fromJson(Map<String, dynamic> json) {
    return HobbitModel(
      isGood: (json['isGood'] as bool),
      title: json['title'],
      hourIndex: (json['hourIndex'] as num).toInt(),
      period: (json['period'] as num).toInt(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isGood': isGood,
      'title': title,
      'hourIndex': hourIndex,
      'period': period,
    };
  }

  factory HobbitModel.fromHobbitEntity(HobbitEntity hobbitEntity) {
    return HobbitModel(
      isGood: hobbitEntity.isGood,
      title: hobbitEntity.name,
      hourIndex: hobbitEntity.hourIndex,
      period: hobbitEntity.period,
    );
  }

  HobbitEntity toHobbitEntity() {
    return HobbitEntity(name: title, period: period, hourIndex: hourIndex, isGood: isGood);
  }
}
