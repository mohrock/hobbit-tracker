// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class HobbitEntity extends DataClass implements Insertable<HobbitEntity> {
  final int id;
  final String name;
  final int period;
  final int hourIndex;
  final bool isGood;
  HobbitEntity(
      {@required this.id,
      @required this.name,
      @required this.period,
      @required this.hourIndex,
      @required this.isGood});
  factory HobbitEntity.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    return HobbitEntity(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      period: intType.mapFromDatabaseResponse(data['${effectivePrefix}period']),
      hourIndex:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}hour_index']),
      isGood:
          boolType.mapFromDatabaseResponse(data['${effectivePrefix}is_good']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || period != null) {
      map['period'] = Variable<int>(period);
    }
    if (!nullToAbsent || hourIndex != null) {
      map['hour_index'] = Variable<int>(hourIndex);
    }
    if (!nullToAbsent || isGood != null) {
      map['is_good'] = Variable<bool>(isGood);
    }
    return map;
  }

  HobbitEntitiesCompanion toCompanion(bool nullToAbsent) {
    return HobbitEntitiesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      period:
          period == null && nullToAbsent ? const Value.absent() : Value(period),
      hourIndex: hourIndex == null && nullToAbsent
          ? const Value.absent()
          : Value(hourIndex),
      isGood:
          isGood == null && nullToAbsent ? const Value.absent() : Value(isGood),
    );
  }

  factory HobbitEntity.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return HobbitEntity(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      period: serializer.fromJson<int>(json['period']),
      hourIndex: serializer.fromJson<int>(json['hourIndex']),
      isGood: serializer.fromJson<bool>(json['isGood']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'period': serializer.toJson<int>(period),
      'hourIndex': serializer.toJson<int>(hourIndex),
      'isGood': serializer.toJson<bool>(isGood),
    };
  }

  HobbitEntity copyWith(
          {int id, String name, int period, int hourIndex, bool isGood}) =>
      HobbitEntity(
        id: id ?? this.id,
        name: name ?? this.name,
        period: period ?? this.period,
        hourIndex: hourIndex ?? this.hourIndex,
        isGood: isGood ?? this.isGood,
      );
  @override
  String toString() {
    return (StringBuffer('HobbitEntity(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('period: $period, ')
          ..write('hourIndex: $hourIndex, ')
          ..write('isGood: $isGood')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(name.hashCode,
          $mrjc(period.hashCode, $mrjc(hourIndex.hashCode, isGood.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is HobbitEntity &&
          other.id == this.id &&
          other.name == this.name &&
          other.period == this.period &&
          other.hourIndex == this.hourIndex &&
          other.isGood == this.isGood);
}

class HobbitEntitiesCompanion extends UpdateCompanion<HobbitEntity> {
  final Value<int> id;
  final Value<String> name;
  final Value<int> period;
  final Value<int> hourIndex;
  final Value<bool> isGood;
  const HobbitEntitiesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.period = const Value.absent(),
    this.hourIndex = const Value.absent(),
    this.isGood = const Value.absent(),
  });
  HobbitEntitiesCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    this.period = const Value.absent(),
    this.hourIndex = const Value.absent(),
    this.isGood = const Value.absent(),
  }) : name = Value(name);
  static Insertable<HobbitEntity> custom({
    Expression<int> id,
    Expression<String> name,
    Expression<int> period,
    Expression<int> hourIndex,
    Expression<bool> isGood,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (period != null) 'period': period,
      if (hourIndex != null) 'hour_index': hourIndex,
      if (isGood != null) 'is_good': isGood,
    });
  }

  HobbitEntitiesCompanion copyWith(
      {Value<int> id,
      Value<String> name,
      Value<int> period,
      Value<int> hourIndex,
      Value<bool> isGood}) {
    return HobbitEntitiesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      period: period ?? this.period,
      hourIndex: hourIndex ?? this.hourIndex,
      isGood: isGood ?? this.isGood,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (period.present) {
      map['period'] = Variable<int>(period.value);
    }
    if (hourIndex.present) {
      map['hour_index'] = Variable<int>(hourIndex.value);
    }
    if (isGood.present) {
      map['is_good'] = Variable<bool>(isGood.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('HobbitEntitiesCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('period: $period, ')
          ..write('hourIndex: $hourIndex, ')
          ..write('isGood: $isGood')
          ..write(')'))
        .toString();
  }
}

class $HobbitEntitiesTable extends HobbitEntities
    with TableInfo<$HobbitEntitiesTable, HobbitEntity> {
  final GeneratedDatabase _db;
  final String _alias;
  $HobbitEntitiesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        minTextLength: 1, maxTextLength: 50);
  }

  final VerificationMeta _periodMeta = const VerificationMeta('period');
  GeneratedIntColumn _period;
  @override
  GeneratedIntColumn get period => _period ??= _constructPeriod();
  GeneratedIntColumn _constructPeriod() {
    return GeneratedIntColumn('period', $tableName, false,
        defaultValue: Constant(24));
  }

  final VerificationMeta _hourIndexMeta = const VerificationMeta('hourIndex');
  GeneratedIntColumn _hourIndex;
  @override
  GeneratedIntColumn get hourIndex => _hourIndex ??= _constructHourIndex();
  GeneratedIntColumn _constructHourIndex() {
    return GeneratedIntColumn('hour_index', $tableName, false,
        defaultValue: Constant(24));
  }

  final VerificationMeta _isGoodMeta = const VerificationMeta('isGood');
  GeneratedBoolColumn _isGood;
  @override
  GeneratedBoolColumn get isGood => _isGood ??= _constructIsGood();
  GeneratedBoolColumn _constructIsGood() {
    return GeneratedBoolColumn('is_good', $tableName, false,
        defaultValue: Constant(false));
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, period, hourIndex, isGood];
  @override
  $HobbitEntitiesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'hobbit_entities';
  @override
  final String actualTableName = 'hobbit_entities';
  @override
  VerificationContext validateIntegrity(Insertable<HobbitEntity> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('period')) {
      context.handle(_periodMeta,
          period.isAcceptableOrUnknown(data['period'], _periodMeta));
    }
    if (data.containsKey('hour_index')) {
      context.handle(_hourIndexMeta,
          hourIndex.isAcceptableOrUnknown(data['hour_index'], _hourIndexMeta));
    }
    if (data.containsKey('is_good')) {
      context.handle(_isGoodMeta,
          isGood.isAcceptableOrUnknown(data['is_good'], _isGoodMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  HobbitEntity map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return HobbitEntity.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $HobbitEntitiesTable createAlias(String alias) {
    return $HobbitEntitiesTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $HobbitEntitiesTable _hobbitEntities;
  $HobbitEntitiesTable get hobbitEntities =>
      _hobbitEntities ??= $HobbitEntitiesTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [hobbitEntities];
}
