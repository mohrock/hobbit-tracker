import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:hobbit_tracker/core/error/exceptions.dart';
import 'package:hobbit_tracker/features/hobbits/data/models/hobbit_model.dart';
import 'package:hobbit_tracker/features/hobbits/domain/entities/hobbit.dart';
import 'package:shared_preferences/shared_preferences.dart';

const LIST_OF_HOBBITS = 'LIST_OF_HOBBITS';

abstract class StorageHobbit {
  Future<List<HobbitModel>> getListOfHobbit();

  Future<void> cacheListOfHobbit(HobbitModel hobbitModel);
}

class StorageHobbitImpl implements StorageHobbit {
  final SharedPreferences sharedPreferences;

  StorageHobbitImpl({@required this.sharedPreferences});

  @override
  Future<void> cacheListOfHobbit(HobbitModel hobbitModel) async {
    List<HobbitModel> output =await getListOfHobbit();
    output.add(hobbitModel);
    return sharedPreferences.setString(
      LIST_OF_HOBBITS,
      json.encode(jsonEncode(output)),
    );
  }

  @override
  Future<List<HobbitModel>> getListOfHobbit() {
    final jsonString = sharedPreferences.getString(LIST_OF_HOBBITS);
    if (jsonString != null) {
      var tagObjsJson = jsonDecode(jsonString);
      List<HobbitModel> tagObjs =
          tagObjsJson.map((tagJson) => HobbitModel.fromJson(tagJson)).toList();
      return Future.value(tagObjs);
    }
    return Future.value([]);
  }
}
