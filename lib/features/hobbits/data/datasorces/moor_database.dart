import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

// Moor works by source gen. This file will all the generated code.
part 'moor_database.g.dart';

@DataClassName('HobbitEntity')
class HobbitEntities extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get name => text().withLength(min: 1, max: 50)();

  IntColumn get period => integer().withDefault(Constant(24))();

  IntColumn get hourIndex => integer().withDefault(Constant(24))();

  BoolColumn get isGood => boolean().withDefault(Constant(false))();
}

@UseMoor(tables: [HobbitEntities])
// _$AppDatabase is the name of the generated class
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      // Specify the location of the database file
      : super((FlutterQueryExecutor.inDatabaseFolder(
          path: 'db.sqlite',
          // Good for debugging - prints SQL in the console
          logStatements: true,
        )));

  // Bump this when changing tables and columns.
  // Migrations will be covered in the next part.
  @override
  int get schemaVersion => 1;

  // All tables have getters in the generated class - we can select the tasks table
  Future<List<HobbitEntity>> getAllHobbits() => (select(hobbitEntities)).get();

  // Moor supports Streams which emit elements when the watched data changes
  Stream<List<HobbitEntity>> watchAllHobbits() => select(hobbitEntities).watch();

  Future insertHobbit(HobbitEntity task) => into(hobbitEntities).insert(task);

  // Updates a Task with a matching primary key
  Future updateHobbit(HobbitEntity task) =>
      update(hobbitEntities).replace(task);

  Future deleteHobbit(HobbitEntity task) => delete(hobbitEntities).delete(task);
}
