import 'package:get_it/get_it.dart';
import 'package:hobbit_tracker/core/utils/time_controller.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/moor_database.dart';
import 'package:hobbit_tracker/features/hobbits/data/datasorces/storage_hobbit.dart';
import 'package:hobbit_tracker/features/hobbits/data/repositories/hobbit_tracker_repository_impl.dart';
import 'package:hobbit_tracker/features/hobbits/domain/repositories/hobbit_tracker_repository.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/add_new_tracker.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/deleteHobbitTracker.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/get_list_of_hobbit_tracker.dart';
import 'package:hobbit_tracker/features/hobbits/domain/usecases/set_done_hobbit_tracker.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/blocs/hobbit_tracker_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Number Trivia
  // Bloc
  sl.registerFactory(
    () => HobbitTrackerCubit(
      getListOfHobbitTracker: sl(),
      addNewTracker: sl(),
      deleteHobbitTracker: sl(),
      setDoneHobbitTracker: sl(),
      timeController: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => SetDoneHobbitTracker(sl()));
  sl.registerLazySingleton(() => DeleteHobbitTracker(sl()));
  sl.registerLazySingleton(() => AddNewTracker(sl()));
  sl.registerLazySingleton(() => GetListOfHobbitTracker(sl()));

  // Repository
  sl.registerLazySingleton<HobbitTrackerRepository>(
      () => HobbitTrackerRepositoryImpl(sl(),sl()));

  sl.registerLazySingleton<StorageHobbit>(
      () => StorageHobbitImpl(sharedPreferences:sl()));

  sl.registerLazySingleton(() => AppDatabase());

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => TimeController());
}
