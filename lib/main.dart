import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/blocs/hobbit_tracker_cubit.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/widgets/hobbit_list.dart';
import 'package:hobbit_tracker/features/hobbits/presentation/widgets/new_hobbit.dart';

import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cubit = di.sl<HobbitTrackerCubit>();
    cubit.getHobbits();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (_) => cubit,
        child: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: BlocBuilder<HobbitTrackerCubit, HobbitTrackerState>(
        builder: (context, state) {
          if (state is Success) {
            return Column(
              children: <Widget>[
                Expanded(child: HobbitList(hobbits: state.hobbits)),
                NewTaskInput(),
              ],
            );
          } else {
            return Column(
              children: <Widget>[
                Expanded(
                    child: Text(
                  'Empty',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                      color: Colors.black54),
                )),
                NewTaskInput(),
              ],
            );
          }
        },
      ),
    );
  }
}
