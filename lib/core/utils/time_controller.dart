
class TimeController {
  int getHourSinceEpochOfDateTime(DateTime dateTime) {
    return dateTime.millisecondsSinceEpoch ~/ (60 * 60 * 1000);
  }
}
